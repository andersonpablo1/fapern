@extends('coord.app')

@section('content')

<div class="form-row text-right">
    <div class="col-10">
        <a class="btn btn-primary" href="{{ url('dashadmin') }}">Voltar</a>
    </div>
 </div>
<br>

<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card">
            <div class="card-header"><h3>Coordenadores</h3></div>
            <div class="card-body">

    @if ($message = Session::get('message'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table id="coord" class="table table-bordered">
        <thead>
        <tr>
            <th>Coordenador</th>
            <th>Nº da Pesquisa</th>
            <th>Data</th>
            <th>Hora</th>
            <th width="350px">Ação</th>
        </tr>
    <thead>
        @foreach ($coord as $cd)
        <tr>
                 
            <td>{{ $cd->user->name}}</td>
            <td>{{ $loop->iteration }}</td>
            <td>{{date('d-m-Y', strtotime($cd->created_at))}}</td>
            <td>{{date('H:i:s', strtotime($cd->created_at))}}</td>
            

           

            <td>
            <form action="{{ route('coord.destroy',$cd->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('coord.show',$cd->id) }}">Visualizar</a>
                    {{-- <a class="btn btn-primary" href="{{ route('coord.edit',$cd->id) }}">Editar</a> --}}

        
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Deletar</button>
                </form>
            </td>
        </tr>
    </tbody>
        @endforeach
    </table>

    {!! $coord->links() !!}

</div>
</div>
</div>
</div>
       
@endsection
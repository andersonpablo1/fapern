@extends('coord.app')

@section('content')

<div class="form-row text-right">
    <div class="col-10">
        <a class="btn btn-primary" href="{{ url('listcoord') }}">Voltar</a>
    </div>
 </div>
<br>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">FORMULÁRIO DE ACOMPANHAMENTO DE PESQUISAS DE INOVAÇÃO – QUESTIONÁRIO DO
                    COORDENADOR </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                                    <form method="POST" action="submit">
                                        @csrf
                                                <div class="form-group ">
                                                <label class="control-label" for="number">
                                                    1. Quantidade de projetos de pesquisa vinculados ao
                                                    orgão?
                                                  
                                                </label>
                                                <input class="form-control" required="true" id="q1" name="q1"
                                                    placeholder="Ex: 1, 2, 3..." required="true" type="text" />
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label">
                                                    2. Todas as pesquisas estão em desenvolvimento de acordo com
                                                    o cronograma?
                                                  
                                                </label>
                                                <div class="">
                                                    <div class="radio" >
                                                        <label class="radio">
                                                            <input name="q2" required="true" required="true" type="radio" value="Sim " />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q2" required="true" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group ">
                                                <label class="control-label requiredField" for="number">
                                                    3. Digite a quantidade de projetos que não está em
                                                    desenvolvimento. (Caso todos os projetos estejam em
                                                    execução, indicar o número 0 no campo abaixo)
                                                  
                                                </label>
                                                <input class="form-control" id="q3" name="q3"
                                                    placeholder="Ex: 0, 1, 2..." required="true" type="text" />
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    4. Houve mudanças no objeto de algum projeto entre a
                                                    aprovação e o início do desenvolvimento?
                                                  
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q4" required="true" type="radio" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q4" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    5. Em qual/quais aspecto/s o projeto teve mudança?
                                                  
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q5[]" type="checkbox" value="Objetivos" />
                                                            Objetivos
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q5[]" type="checkbox"
                                                                value="Referencial teórico" />
                                                            Referencial teórico
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q5[]" type="checkbox" value="Metodologia" />
                                                            Metodologia
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q5[]" type="checkbox" value="Cronograma" />
                                                            Cronograma
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q5[]" type="checkbox"
                                                                value="Não houve mudança" />
                                                            Não houve mudança
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>




                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    6.Tem sido possível acompanhar/orientar adequadamente as
                                                    pesquisas?
                                                  
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q6[]" type="checkbox" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q6[]" type="checkbox" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q6[]" type="checkbox" value="Em partes" />
                                                            Em partes
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    7. Caso não tenha sido possível acompanhar
                                                    adequadamente as pesquisas, informe o/os motivo/s. (múltipla
                                                    escolha)
                                                  
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q7[]" type="checkbox"
                                                                value="Ausência do pesquisador nas orientações " />
                                                            Ausência do pesquisador nas orientações
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q7[]" type="checkbox"
                                                                value="Não entrega do relatório parcial/final" />
                                                            Não entrega do relatório parcial/final
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q7[]" type="checkbox"
                                                                value="Não cumprimento da carga horária estipulado em edital" />
                                                            Não cumprimento da carga horária estipulado em
                                                            edital
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q7[]" type="checkbox" value="Outros" />
                                                            Outros
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    8. A carga horária semanal do pesquisador é suficiente
                                                    para o desenvolvimento do projeto de pesquisa?
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q8" required="true" type="radio" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q8" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    9. Qual a frequência de encontros de orientação?
                                                  
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q9[]" type="checkbox" value="Diário" />
                                                            Diário
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q9[]" type="checkbox" value="Semanal" />
                                                            Semanal
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q9[]" type="checkbox" value="Quinzenal" />
                                                            Quinzenal
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q9[]" type="checkbox" value="Mensal" />
                                                            Mensal
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q9[]" type="checkbox" value="Outro" />
                                                            Outro
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    10. O/os relatório/s foi/foram produzido/s?   
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q10" required="true" type="radio" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q10" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    11. Indique o nível de satisfação relacionado
                                                    &agrave; inovação proporcionada ao órgão
                                                  
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q11" required="true" type="radio" value="Excelente" />
                                                            Excelente
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q11" required="true" type="radio" value="Bom" />
                                                            Bom
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q11" required="true" type="radio" value="Regular" />
                                                            Regular
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q11" required="true" type="radio" value="Ruim" />
                                                            Ruim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q11" required="true" type="radio" value="Péssimo" />
                                                            Péssimo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    12. Indique o nível de satisfação relacionado
                                                    ao
                                                    desempenho do pesquisador.
                                                  
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q12" required="true" type="radio" value="Excelente" />
                                                            Excelente
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q12" required="true" type="radio" value="Bom" />
                                                            Bom
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q12" required="true" type="radio" value="Regular" />
                                                            Regular
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q12" required="true" type="radio" value="Ruim" />
                                                            Ruim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q12" required="true" type="radio" value="Péssimo" />
                                                            Péssimo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label " for="q13">
                                                    13. Espaço reservado para anotações sobre as
                                                    questões acima, sugestões e dúvidas em geral.
                                                    (OPTATIVO)
                                                </label>
                                                <textarea class="form-control" cols="40" id="q13" name="q13"
                                                    placeholder="Questão dissertativa" rows="10"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div>
                                                    <button class="btn btn-primary " name="submit" type="submit">
                                                        Enviar
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endsection
@extends('../layouts.admin')

@section('content')
<title>FAPERN</title>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
            </div>
            <div class="pull-right">
                <button onClick="window.print()" a class="btn btn-primary" >Imprimir Formulário</button></a>
                <a class="btn btn-primary" href="{{ route('coord.index') }}">Voltar</a>
            </div>
        </div>
    </div>

    <table class="table" .table> 
        <tr>
            <td>
<h5>Olá, {{$coord->user->name}}!<br>CPF: {{$coord->user->cpf}}<br>E-mail: {{$coord->user->email}}<br>{{$coord->user->edital}}</h5>
            </td>       
</tr>
<td>
<tr>
    <h1>Comprovante de Inscrição de Nº: {{$coord->id}}</h1>
</td>
</tr>
</table>
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>1. Quantidade de projetos de pesquisa vinculados ao órgão?</strong><br>
                <strong>Reposta:</strong> {{ $coord->q1 }}
            </div>
        </div>

       
        <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>2.	Todas as pesquisas estão em desenvolvimento de acordo com o cronograma?</strong><br>
<strong>Reposta:</strong> {{ $coord->q2}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>3. Digite a quantidade de projetos que não está em desenvolvimento. </strong>(Caso todos os projetos estejam em execução, indicar o número 0 no campo abaixo).<br>
<strong>Reposta:</strong> {{ $coord->q3}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>4. Houve mudanças no objeto de algum projeto entre a aprovação e o início do desenvolvimento?</strong><br>
<strong>Reposta:</strong> {{ $coord->q4}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>5. Em qual/quais aspecto/s os projetos tiveram mudanças?</strong> (múltipla escolha)<br>
<strong>Reposta:</strong> {{ $coord->q5}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>6. Tem sido possível acompanhar/orientar adequadamente as pesquisas? </strong><br>
<strong>Reposta:</strong> {{ $coord->q6}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>7.	 Caso não tenha sido possível acompanhar adequadamente as pesquisas, informe o/os motivo/s.</strong> (múltipla escolha)<br>
<strong>Reposta:</strong> {{ $coord->q7}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>8. A carga horária semanal do pesquisador é suficiente para o desenvolvimento do projeto de pesquisa?</strong><br>
<strong>Reposta:</strong> {{ $coord->q8}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>9. Qual a frequência de encontros de orientação?</strong><br>
<strong>Reposta:</strong> {{ $coord->q9}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>10. O/os relatório/s foi/foram produzido/s?</strong><br>
<strong>Reposta:</strong> {{ $coord->q10}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>11.	Indique o nível de satisfação relacionado à inovação proporcionada ao órgão</strong><br>
<strong>Reposta:</strong> {{ $coord->q11}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>12.	Indique o nível de satisfação relacionado ao desempenho do pesquisador.</strong><br>
<strong>Reposta:</strong> {{ $coord->q12}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>13.	Espaço reservado para anotações sobre as questões acima, sugestões e dúvidas em geral. </strong><br>
<strong>Reposta:</strong> {{ $coord->q13}}
</div>
</div>

@endsection
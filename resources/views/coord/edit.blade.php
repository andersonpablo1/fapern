@extends('../layouts.admin')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Coordenadores</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('coord.index') }}">Voltar</a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Ops!</strong> Algum problema ocorreu com sua entrada<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('coord.update',$coord->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>q1:</strong>
                    <input type="text" name="q1" value="{{ $coord->q1 }}" class="form-control" placeholder="q1">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q2:</strong>
<input type="text" name="q2" value="{{ $coord->q2 }}" class="form-control" placeholder="q2">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q3:</strong>
<input type="text" name="q3" value="{{ $coord->q3 }}" class="form-control" placeholder="q3">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q4:</strong>
<input type="text" name="q4" value="{{ $coord->q4 }}" class="form-control" placeholder="q4">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q5:</strong>
<input type="text" name="q5" value="{{ $coord->q5 }}" class="form-control" placeholder="q5">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q6:</strong>
<input type="text" name="q6" value="{{ $coord->q6 }}" class="form-control" placeholder="q6">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q7:</strong>
<input type="text" name="q7" value="{{ $coord->q7 }}" class="form-control" placeholder="q7">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q8:</strong>
<input type="text" name="q8" value="{{ $coord->q8 }}" class="form-control" placeholder="q8">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q9:</strong>
<input type="text" name="q9" value="{{ $coord->q9 }}" class="form-control" placeholder="q9">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q10:</strong>
<input type="text" name="q10" value="{{ $coord->q10 }}" class="form-control" placeholder="q10">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q11:</strong>
<input type="text" name="q11" value="{{ $coord->q11 }}" class="form-control" placeholder="q11">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q12:</strong>
<input type="text" name="q12" value="{{ $coord->q12 }}" class="form-control" placeholder="q12">
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>q13:</strong>
<input type="text" name="q13" value="{{ $coord->q13 }}" class="form-control" placeholder="q13">
</div>
</div>
            <div class="'col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection
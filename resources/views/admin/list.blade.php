@extends('../coord.app')

@section('content')

<div class="form-row text-right">
    <div class="col-10">
        <a class="btn btn-primary" href="{{ url('dashadmin') }}">Voltar</a>
    </div>
 </div>
<br>

 
<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card">
            <div class="card-header"><h3>Usuários Cadastrados</h3></div>
            <div class="card-body">
    
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table table-bordered">
        <tr>
            <th>No</th>
            <th>Nome</th>
            <th>CPF</th> 
            <th>E-mail</th> 
            <th>Telefone</th> 
            <th>Função</th> 
            <th>Edital</th>    
            <th width="350px">Ação</th>
        </tr>
        @foreach ($admin as $adm)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $adm->name }}</td>
            <td>{{ $adm->cpf }}</td>
            <td><a href="mailto:{{$adm->email}}">{{$adm->email}}</a></td>
            <td>{{ $adm->telefone }}</td>
            <td>{{ $adm->funcao }}</td>
            <td>{{ $adm->edital }}</td>
            <td>
                <form action="{{ route('admin.destroy',$adm->id) }}" method="POST">
   
                    {{-- <a class="btn btn-info" href="{{ route('admin.show',$adm->id) }}">Mostrar</a> --}}
    
                    <a class="btn btn-primary" href="{{ route('admin.edit',$adm->id) }}">Editar</a>
    
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Deletar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
        
@endsection
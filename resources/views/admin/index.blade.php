@extends('admin.layout')


<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


@section('content')
    
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-bookmark"></span> Menu</h3>
                </div>
                <div class="panel-body">
                    <div class="row ">
                       
                      <div class="col-xs-8 col-md-12">
                        <a href="{{ url('pesq') }}" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-user"></span> <br/>Pesquisador</a>
                        <a href="{{ url('coord') }}" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-file"></span> <br/>Coordenadores</a>
                        <a href="{{ url('admin') }}" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-th-list"></span> <br/>Todos Usuários</a>
                        <a href="#" class="btn btn-warning btn-lg" role="button"><span class="glyphicon glyphicon-stats"></span> <br/>Gráficos</a>
                        <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-th-list"></span> <br/>E-mails</a>
                        <a href="#" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-file"></span> <br/>Estatística</a>

                      </div>
                      
                      {{-- <div class="col-xs-6 col-md-6">
                          <a href="#" class="btn btn-danger btn-lg" role="button"><span class="glyphicon glyphicon-list-alt"></span> <br/>Apps</a>
                          <a href="#" class="btn btn-warning btn-lg" role="button"><span class="glyphicon glyphicon-bookmark"></span> <br/>Bookmarks</a>
                          <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-signal"></span> <br/>Reports</a>
                          <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-comment"></span> <br/>Comments</a>
                        </div> --}}
                       
                    </div>
                    {{-- <a href="#" class="btn btn-success btn-lg btn-block" role="button"><span class="glyphicon glyphicon-globe"></span> Website</a> --}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



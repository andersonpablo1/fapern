@extends('admin.app')
   
@section('content')

           
 <br>
                
 <div class="pull-center">
    <a class="btn btn-primary" href="{{url()->previous()}}">Voltar</a>
</div>

<br>
           
<div class="row justify-content-center">
    <div class="col-4" align-self-center”>
        <div class="card">
            <div class="card-header"><h3>Editar Usuário</h3></div>
            <div class="card-body">       
            </div>
        </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Ops!</strong>Aconteceu algum erro com sua entrada.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('admin.update',$admin->id) }}" method="POST">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Nome:</strong>
                    <input type="text" name="name" value="{{ $admin->name }}" class="form-control" placeholder="Name">
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>CPF:</strong>
                            <input type="number" name="cpf" value="{{ $admin->cpf }}" class="form-control" placeholder="CPF">
                        </div>

         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Telefone:</strong>
                    <input type="text" name="telefone" value="{{ $admin->telefone }}" class="form-control" placeholder="Telefone">
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>E-mail:</strong>
                            <input type="text" name="email" value="{{ $admin->email }}" class="form-control" placeholder="Email">
                        </div>

                        
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Função:</strong>
                            <input type="text" name="funcao" value="{{ $admin->funcao }}" class="form-control" placeholder="Função">
                        </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Edital:</strong>
                                <input type="text" name="edital" value="{{ $admin->edital }}" class="form-control" placeholder="Edital">
                            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Enviar</button>
              <br><br>
            </div>
        </div>
      

    </form>
@endsection
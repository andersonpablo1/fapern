@extends('pesq.app')

@section('content')

<div class="form-row text-right">
    <div class="col-10">
        <a class="btn btn-primary" href="{{ url('listpesq') }}">Voltar</a>
    </div>
 </div>
<br>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">FORMULÁRIO DE ACOMPANHAMENTO DE PESQUISAS DE INOVAÇÃO – QUESTIONÁRIO DO
                    PESQUISADOR </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                    @endif
                       <form method="POST" action="submit1">
                       @csrf
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    1. A pesquisa já foi iniciada?
                                                
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q1" required="true" type="radio" value="Sim " />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q1" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    2. Em caso de a pesquisa não ter sido iniciada indique o
                                                    motivo
                                                   
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q2[]" type="checkbox"
                                                                value="Falta de assinatura de termo de compromisso" />
                                                            Falta de assinatura de termo de compromisso
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q2[]" type="checkbox"
                                                                value="Inexistência de condições técnicas de trabalho" />
                                                            Inexistência de condições
                                                            técnicas de trabalho
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q2[]" type="checkbox"
                                                                value="Realocação do bolsista" />
                                                            Realocação do bolsista
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q2[]" type="checkbox"
                                                                value="Atraso no repasse da bolsa" />
                                                            Atraso no repasse da bolsa
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q2[]" type="checkbox"
                                                                value="A pesquisa foi iniciada" />
                                                            A pesquisa foi iniciada
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    3. Houve mudanças no objeto de algum projeto entre a
                                                    aprovação e o início do desenvolvimento?
                                                   
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q3" required="true" type="radio" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q3" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    4. Em qual/quais aspecto/s o projeto teve mudança?
                                                   
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q4[]" type="checkbox" value="Objetivos" />
                                                            Objetivos
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q4[]" type="checkbox"
                                                                value="Referencial teórico" />
                                                            Referencial teórico
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q4[]" type="checkbox" value="Metodologia" />
                                                            Metodologia
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q4[]" type="checkbox" value="Cronograma" />
                                                            Cronograma
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q4[]" type="checkbox"
                                                                value="Não houve mudança" />
                                                            Não houve mudança
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    5. Em qual fase de execução encontra-se a pesquisa?
                                                   
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q5[]" type="checkbox" value="Fase Inicial" />
                                                            Fase Inicial
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q5[]" type="checkbox"
                                                                value="Fundamentação Teórica" />
                                                            Fundamentação Teórica
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q5[]" type="checkbox"
                                                                value="Constituição/produção dos dados" />
                                                            Constituição/produção dos dados
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q5[]" type="checkbox"
                                                                value="Fase de conclusão" />
                                                            Fase de conclusão
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    6. Será necessária prorrogação de prazo
                                                    para conclusão da pesquisa?
                                                   
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q6" required="true" type="radio" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q6" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    7. Qual motivo leva à solicitação de
                                                    prorrogação?
                                                   
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q7[]" type="checkbox"
                                                                value="Problemas de saúde" />
                                                            Problemas de saúde
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q7[]" type="checkbox"
                                                                value="Falta de orientação" />
                                                            Falta de orientação
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q7[]" type="checkbox"
                                                                value="Mudanças no projeto" />
                                                            Mudanças no projeto
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q7[]" type="checkbox"
                                                                value="Sobrecarga de trabalho na pesquisa" />
                                                            Sobrecarga de trabalho na pesquisa
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q7[]" type="checkbox"
                                                                value="Não há motivo para prorrogação " />
                                                            Não há motivo para prorrogação
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    8. A pesquisa está sendo acompanhada/orientada?
                                                   
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q8" required="true" type="radio" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q8" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    9. Quem coordena a pesquisa?
                                                   
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q9[]" type="checkbox"
                                                                value="Coordenador do próprio órgão no qual a pesquisa está sendo executada" />
                                                            Coordenador do próprio órgão no qual a
                                                            pesquisa está sendo executada
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q9[]" type="checkbox"
                                                                value="Coordenador de outros órgãos do Governo do Estado" />
                                                            Coordenador de outros órgãos do Governo do
                                                            Estado
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q9[]" type="checkbox"
                                                                value="Pessoa externa aos quadros do Estado" />
                                                            Pessoa externa aos quadros do Estado
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q9[]" type="checkbox"
                                                                value="Não há acompanhamento " />
                                                            Não há acompanhamento
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    10. A carga horária semanal é suficiente para o
                                                    desenvolvimento do projeto de pesquisa?
                                                   
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q10" required="true" type="radio" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q10" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    11. Qual a periodicidade do acompanhamento por parte do coordenador?
                                                   
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q11[]" type="checkbox" value="Diário" />
                                                            Diário
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q11[]" type="checkbox" value="Semanal" />
                                                            Semanal
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q11[]" type="checkbox" value="Quinzenal" />
                                                            Quinzenal
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q11[]" type="checkbox" value="Mensal" />
                                                            Mensal
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q11[]" type="checkbox" value="Outro" />
                                                            Outro
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    12. O/os relatário/s foi/foram produzido/s?
                                                   
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q12" required="true" type="radio" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q12" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    13. Qual/quais o/os motivo/s que levaram a não entrega do
                                                    relatório?
                                                   
                                                </label>
                                                <div class=" ">
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q13[]" type="checkbox"
                                                                value="Falta de orientação" />
                                                            Falta de orientação
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q13[]" type="checkbox"
                                                                value="Mudanças no projeto" />
                                                            Mudanças no projeto
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q13[]" type="checkbox" value="Outros " />
                                                            Outros
                                                        </label>
                                                    </div>
                                                    <div class="checkbox">
                                                        <label class="checkbox">
                                                            <input name="q13[]" type="checkbox"
                                                                value="Os relatórios foram entregues" />
                                                            Os relatórios foram entregues
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    14. Há clareza a quem o pesquisador deve se dirigir para
                                                    sanar dúvidas administrativas do cotidiano (frequência,
                                                    ajustes de horários, eventos, reuniões, etc)?
                                                   
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q14" required="true" type="radio" value="Sim" />
                                                            Sim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q14" required="true" type="radio" value="Não" />
                                                            Não
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    15. Indique o nível de satisfação relacionado
                                                    à execução do proejo de pesquisa.
                                                   
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q15" required="true" type="radio" value="Excelente" />
                                                            Excelente
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q15" required="true" type="radio" value="Bom" />
                                                            Bom
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q15" required="true" type="radio" value="Regular" />
                                                            Regular
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q15" required="true" type="radio" value="Ruim" />
                                                            Ruim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q15" required="true" type="radio" value="Péssimo" />
                                                            Péssimo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label requiredField">
                                                    16. Indique o nível de satisfação relacionado
                                                    ao acompanhamento do projeto pelo coordenador.
                                                   
                                                </label>
                                                <div class="">
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q16" required="true" type="radio" value="Excelente" />
                                                            Excelente
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q16" required="true" type="radio" value="Bom" />
                                                            Bom
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q16" required="true" type="radio" value="Regular" />
                                                            Regular
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q16" required="true" type="radio" value="Ruim" />
                                                            Ruim
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="radio">
                                                            <input name="q16" required="true" type="radio" value="Péssimo" />
                                                            Péssimo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <label class="control-label " for="q17">
                                                    17. Espaço reservado para anotações sobre as
                                                    questões acima, sugestões e dúvidas em geral.
                                                    (OPTATIVO)
                                                </label>
                                                <textarea class="form-control form-control" cols="40" id="q12"
                                                    name="q17" placeholder="Questão dissertativa"
                                                    rows="10"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div>
                                                    <button class="btn btn-primary " name="submit1" type="submit">
                                                        Enviar
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endsection

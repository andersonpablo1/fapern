@extends('../layouts.admin')

@section('content')
<title>FAPERN</title>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
            </div>
            <div class="pull-right">
                <button onClick="window.print()" a class="btn btn-primary" >Imprimir Formulário</button></a>
                <a class="btn btn-primary" href="{{url()->previous()}}">Voltar</a>
            </div>  
        </div>
    </div>

    <table class="table" .table> 
        <tr>
            <td>
<h5>Olá, {{$pesq->user->name}}!<br>CPF: {{$pesq->user->cpf}}<br>E-mail: {{$pesq->user->email}}<br>{{$pesq->user->edital}}</h5>
            </td>       
</tr>
<td>
<tr>
    <h1>Comprovante de Inscrição de Nº: {{$pesq->id}}</h1>
</td>
</tr>
</table>
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>1. A pesquisa já foi iniciada?</strong><br>
                <strong>Reposta:</strong> {{ $pesq->q1 }}
            </div>
        </div>

       
        <div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>2. Em caso de a pesquisa não ter sido iniciada indique o motivo.</strong><br>
<strong>Reposta:</strong> {{ $pesq->q2}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>3. Houve mudanças no objeto do projeto entre a aprovação e o início do desenvolvimento?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q3}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>4. Em qual/quais aspecto/s o projeto teve mudança?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q4}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>5. Em qual fase de execução encontra-se a pesquisa?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q5}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>6. Será necessária prorrogação de prazo para conclusão da pesquisa?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q6}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>7. Qual motivo leva à solicitação de prorrogação? </strong><br>
<strong>Reposta:</strong> {{ $pesq->q7}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>8.	A pesquisa está sendo acompanhada/orientada? </strong><br>
<strong>Reposta:</strong> {{ $pesq->q8}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>9.	Quem coordena a pesquisa?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q9}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>10.	A carga horária semanal é suficiente para o desenvolvimento do projeto de pesquisa?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q10}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>11.	Qual a periodicidade do acompanhamento por parte do coordenador?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q11}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>12.	O/os relatório/s foi/foram produzido/s?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q12}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>13.	Qual/quais o/os motivo/s que levaram a não entrega do relatório?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q13}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>14.	Há clareza a quem o pesquisador deve se dirigir para sanar dúvidas administrativas do cotidiano (frequência, ajustes de horários, eventos, reuniões, etc)?</strong><br>
<strong>Reposta:</strong> {{ $pesq->q14}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>15.	Indique o nível de satisfação relacionado à execução do proejo de pesquisa.</strong><br>
<strong>Reposta:</strong> {{ $pesq->q15}}
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="form-group">
<strong>16.	Indique o nível de satisfação relacionado ao acompanhamento do projeto pelo coordenador.</strong><br>
<strong>Reposta:</strong> {{ $pesq->q16}}
</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
    <strong>17.	Qual/quais o/os motivo/s que levaram a não entrega do relatório?</strong><br>
    <strong>Reposta:</strong> {{ $pesq->q17}}
    </div>
    </div>
@endsection
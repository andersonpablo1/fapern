@extends('pesq.app')

@section('content')

  <div class="form-row text-right">
    <div class="col-10">
        <a class="btn btn-primary" href="{{ route('createpesq') }}">Responder Questionário</a>
    </div>
 </div>
<br>

<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card">
            <div class="card-header"><h3>Meu Questionário</h3></div>
            <div class="card-body">

    @if ($message = Session::get('message'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table id="pesq" class="table table-bordered">
        <thead>
        <tr>
            <th>Pesquisador</th>
            <th>Nº da Pesquisa</th>
            <th>Data</th>
            <th>Hora</th>
            <th width="350px">Ação</th>
        </tr>
    <thead>
        @foreach ($pesq as $ps)
        <tr>
                 
            <td>{{ $ps->user->name}}</td>
            <td>{{ $loop->iteration }}</td>
            <td>{{date('d-m-Y', strtotime($ps->created_at))}}</td>
            <td>{{date('H:i:s', strtotime($ps->created_at))}}</td>
            

           

            <td>
            <form action="{{ route('destroypesq',$ps->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('showpesq',$ps->id) }}">Visualizar</a>
        
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Deletar</button>
                </form>
            </td>
        </tr>
    </tbody>
        @endforeach
    </table>
</div>
</div>
</div>
</div>
       
@endsection
@extends('layouts.app')

@section('content')
{{-- 
<script type="text/javascript" src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script> --}}

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.0/jquery.mask.js"></script>

<script type="text/javascript">
$(document).ready(function(){
        $("#cpf").mask("999.999.999-99");
    $('#telefone').mask('(00) 00000-0000');
});
</script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registrar') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <!--FORM CPF-->

                        <div class="inputs">
                        <div class="form-group row">
                            <label for="cpf" class="col-md-4 col-form-label text-md-right">{{ __('CPF') }}</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control cpf-mask @error('cpf') is-invalid @enderror" cpf-mask="000.000.000-00" id="cpf" name="cpf" placeholder="Ex.: 000.000.000-00" maxlength="14"value="{{ old('cpf') }}" required autocomplete="cpf" autofocus>
                                    @error('cpf')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                        <!--FORM TELEFONE-->


                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Telefone') }}</label>

                            <div class="col-md-6">
                                <input id="telefone" type="text"
                                    class="form-control @error('telefone') is-invalid @enderror" name="telefone"
                                    value="{{ old('telefone') }}" placeholder="Ex.: (00) 00000-0000" required autocomplete="telefone" autofocus>

                                @error('telefone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <!--FORM EMAIL-->

                        <div class="form-group row">
                            <label for="email"
                                class="col-md-4 col-form-label text-md-right">{{ __('Endereço de E-mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <!--FORM COORDENADOR OU BOLSISTA-->


                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right"></label>
                            <div class="col-md-6">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="funcao"
                                        id="inlineRadio1" value="Coordenador" required>
                                    <label class="form-check-label" for="inlineRadio1">Coordenador</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="funcao"
                                        id="inlineRadio2" value="Pesquisador">
                                    <label class="form-check-label" for="inlineRadio2">Pesquisador</label>
                                </div>
                            </div>
                        </div>



                        <!--FIM ALUNO OU BOLSISTA-->



                        <!--FORM EDITAL-->





                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Edital') }}</label>
                            <div class="col-md-6">
                                <select class="select form-control" id="edital" name="edital" requiredField>
                                    <option value="Edital Nº 01/2019 FAPERN-SEMARH">
                                        1. Edital Nº 01/2019 FAPERN-SEMARH
                                    </option>
                                    <option value="Edital: Nº 02/2019 FAPERN-SEMARH">
                                        2. Edital Nº 02/2019 FAPERN-SEMARH
                                    </option>
                                    <option value="Edital: Nº 01/2019 FAPERN-IGARN">
                                        3. Edital Nº 01/2019 FAPERN-IGARN
                                    </option>
                                    <option value="Edital: Nº 02/2018 FAPERN-SEAD/EGRN">
                                        4. Edital Nº 02/2018 FAPERN-SEAD/EGRN
                                    </option>
                                    <option value="Edital: Nº 04/2019 FAPERN-SEAD/EGRN">
                                        5. Edital Nº 04/2019 FAPERN-SEAD/EGRN
                                    </option>
                                    <option value="Edital: Nº 01/2019 FAPERN-SETHAS">
                                        6. Edital Nº 01/2019 FAPERN-SETHAS
                                    </option>
                                    <option value="Edital: Nº 01/2020 FAPERN-IPERN">
                                        7. Edital Nº 01/2020 FAPERN-IPERN
                                    </option>
                                    <option value="Edital: Nº 01/2018 FAPERN-SEEC (PIBICjr)">
                                        8. Edital Nº 01/2018 FAPERN-SEEC (PIBICjr)
                                    </option>
                                    <option
                                        value="9. Edital Nº 02/2018 FAPERN/Espaço Ciência (Eureka)">
                                        9. Edital Nº 02/2018 FAPERN/Espaço Ciência
                                        (Eureka)
                                    </option>
                                </select>
                            </div>
                        </div>

                        <!--FIM FORM EDITAL-->


                        <div class="form-group row">
                            <label for="password"
                                class="col-md-4 col-form-label text-md-right">{{ __('Senha') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Senha') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                                       
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>  
    </div>
</div>

@endsection


<?php

 
Auth::routes(); 
Route::get('/','HomeController@index');
Route::view('createcoord','coord/create')->name('createcoord')->middleware('coord'); //Formulário Coordenador
Route::post('submit','CoordenadorController@store')->middleware('coord');  //Salvar dados do Coordenador no Banco de Dados
Route::get('listcoord','CoordenadorController@listCoord')->name('listcoord')->middleware('coord');  //Página do Coordenador para o Coordenador
Route::get('showcoord/{id}','CoordenadorController@showCoord')->name('showcoord')->middleware('coord'); //Ação de mostrar dados apenas para coordenador
Route::view('createpesq','pesq/create')->name('createpesq')->middleware ('pesq'); //Formulário do Pesquisador
Route::post('submit1','PesquisadorController@store')->middleware('pesq'); //Salvar dados do Pesquisador no Banco de Dados
Route::get('listpesq','PesquisadorController@listUser')->name('listpesq')->middleware('pesq');  //Página do pesquisador para o Pesquisador
Route::get('showpesq/{id}','PesquisadorController@showPesq')->name('showpesq')->middleware('pesq'); //Ação de mostrar dados apenas para pesquisador
Route::resource('dashadmin','ViewController');//->middleware('admin'); //DashBoard do Admin
Route::resource('admin','AdminController')->middleware('admin');
Route::resource('coord','CoordenadorController')->middleware('admin'); //Página do Coordenador para o ADM
Route::resource('pesq','PesquisadorController')->middleware('admin'); //Página do pesquisador para o ADM
Route::delete('deletecoord/{id}','CoordenadorController@destroyCoord')->name('destroycoord')->middleware('coord'); //Ação de mostrar dados apenas para coordenador
Route::delete('deletepesq/{id}','PesquisadorController@destroyPesq')->name('destroypesq')->middleware('pesq'); //Ação de mostrar dados apenas para coordenador

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordenador extends Model
{
    protected $table = "coordenador";

    protected $fillable = [
        'user_id',   
        'q1',
        'q2',
        'q3',
        'q4',
        'q5',
        'q6',
        'q7',
        'q8',
        'q9',
        'q10',
        'q11',
        'q12',
        'q13',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

}
 
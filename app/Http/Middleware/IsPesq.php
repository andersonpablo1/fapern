<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class IsPesq {
    public function handle($request, Closure $next)
    {
     if (Auth::user() &&  Auth::user()->funcao == 'Pesquisador') {
            return $next($request);
     }
    return redirect('/');
    }
}
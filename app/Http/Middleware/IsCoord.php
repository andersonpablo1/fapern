<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;



class IsCoord
{
    public function handle($request, Closure $next)
    {
     if (Auth::user() &&  Auth::user()->funcao == 'Coordenador') {
            return $next($request);
     }

    return redirect('/');
}
}

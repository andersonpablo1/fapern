<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth; 

class HomeController extends Controller {

    public function index() {

        if (Auth::check()) {
 
        if (auth()->user()->funcao == 'Coordenador') { 
            return redirect()->route('listcoord');
        } elseif  (auth()->user()->funcao == 'Pesquisador') {
            return redirect()->route('listpesq');
        } elseif  (auth()->user()->funcao == 'Admin') {
            return redirect()->route('dashadmin');      
        }
    }
    return view('welcome');
    }
}

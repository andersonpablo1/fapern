<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    use AuthenticatesUsers;

   // protected $redirectTo = RouteServiceProvider::HOME;
    protected function redirectTo()     {
        if (auth()->user()->funcao == 'Coordenador') { //verificar se respondeu e redirecionar pra o form ou não
            return '/listcoord';
        } elseif  (auth()->user()->funcao == 'Pesquisador') {
            return '/listpesq';
    }  
        elseif  (auth()->user()->funcao == 'Admin') {
            return '/dashadmin';
        }
    return '/';
}

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}

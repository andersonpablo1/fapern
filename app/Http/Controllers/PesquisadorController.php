<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Pesquisador;
use Illuminate\Support\Facades\Auth; 

class PesquisadorController extends Controller{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {

        $pesq = Pesquisador::latest()->paginate(5);
          return view('pesq.list',compact('pesq'))
            ->with('i', (request()->input('page', 1) - 1) * 5);      
       }
 
     public function listUser() { 
        $userId = Auth::id();
        $pesq = Pesquisador::where('user_id',$userId)->orderBy('id', 'ASC')->get();
        return view('pesq.list_user', compact('pesq'));
       }
  
    public function store(Request $request) {

        $data =  $request->input();
        $data['q2'] = implode(", ",$data['q2']);
        $data['q4'] = implode(", ",$data['q4']);
        $data['q5'] = implode(", ",$data['q5']);
        $data['q7'] = implode(", ",$data['q7']);
        $data['q9'] = implode(", ",$data['q9']);
        $data['q11'] = implode(", ",$data['q11']);
        $data['q13'] = implode(", ",$data['q13']);
        $data['user_id'] = Auth::id();
        $pesquisador = Pesquisador::create($data);
        
        //dd($pesquisador,$pesquisador->user());
       return redirect()->route('listpesq')
       ->with('message','Questionário respondido com Sucesso');
    }  
    
    public function show(Pesquisador $pesq){
        return view('pesq.show',compact('pesq'));
}

    public function showPesq($id) { 
        $userId = Auth::id();
        $pesq = Pesquisador::where('id', $id)->where('user_id', $userId)->firstOrFail();
        return view('pesq.show_pesq',compact('pesq'));        
    }

    public function edit(Pesquisador $pesq){
        return view('pesq.edit',compact('pesq'));
    }

    public function update(Request $request, Pesquisador $pesq)
    {
        $request->validate([
            'q1' => 'required',
            'q2' => 'required', 
            'q3' => 'required',
            'q4' => 'required', 
            'q5' => 'required',
            'q6' => 'required', 
            'q7' => 'required',
            'q8' => 'required', 
            'q9' => 'required',
            'q10' => 'required', 
            'q11' => 'required',
            'q12' => 'required',       
            'q13' => 'required', 
            'q14' => 'required',       
            'q15' => 'required', 
            'q16' => 'required',       
            'q17' => 'required',      
        ]);

        $pesq->update($request->all());

        
        return redirect()->route('pesq.index')
                        ->with('success','Usuário Alterado com Sucesso');

    }
    
   public function destroy(Pesquisador $pesq) {
       $pesq->delete();
 
       return redirect()->route('pesq.index')
                       ->with('message','Coordenador excluído com Sucesso');
    }

    public function destroyPesq($id) { 
        $userId = Auth::id();
        $pesq = Pesquisador::where('id', $id)->where('user_id', $userId)->firstOrFail();
        $pesq->delete();
        return redirect()->route('listpesq')
        ->with('message','Questionário excluído com Sucesso');      
       }
    }

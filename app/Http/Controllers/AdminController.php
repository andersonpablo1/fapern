<?php

namespace App\Http\Controllers;

use App\Administrador;
use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller{


    public function __construct(){
        $this->middleware('auth');
    }


    public function index() {
    //    $admin = User::latest()->paginate(5);
    //      return view('admin.list',compact('admin'))
    //        ->with('i', (request()->input('page', 1) - 1) * 5);

           $admin = User::all();

           return view('admin.list', compact('admin'));
    }

    public function show(Administrador $admin){
        return view('admin.show',compact('admin'));
    }

    public function edit(Administrador $admin){
        return view('admin.edit',compact('admin'));
    }

    public function update(Request $request, Administrador $admin)
    {
        $request->validate([
            'name' => 'required',
            'cpf' => 'required',
            'funcao' => 'required',
            'edital' => 'required',
            'telefone' => 'required',
            'email' => 'required',
        ]);

        $admin->update($request->all());

        
        return redirect()->route('admin.index')
                        ->with('success','Usuário Alterado com Sucesso');

    }

    public function destroy(Administrador $admin) {
        $admin->delete();
  
        return redirect()->route('admin.index')
                        ->with('Sucesso','Adm excluído com Sucesso');
    }
}
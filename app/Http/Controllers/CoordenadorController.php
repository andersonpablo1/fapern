<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Coordenador;
use Illuminate\Support\Facades\Auth; 


class CoordenadorController extends Controller {


    public function __construct() {
        $this->middleware('auth');
    }


    public function index() {

        $coord = Coordenador::latest()->paginate(5);

        return view('coord.list',compact('coord'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
     }

       // Função pra Listar os dados do user logado
       public function listCoord() { 
        $userId = Auth::id();
        $coord = Coordenador::where('user_id',$userId)->get();
        return view('coord.list_coord', compact('coord'));
        
       }

    public function store(Request $request) {

        $data = $request->input();
        $data['q5'] = implode(", ",$data['q5']);
        $data['q6'] = implode(", ",$data['q6']);
        $data['q7'] = implode(", ",$data['q7']);
        $data['q9'] = implode(", ",$data['q9']);
        $data['user_id'] = Auth::id();
        $coordenador = Coordenador::create($data);
       // dd($coordenador, $coordenador->user);

    return redirect()->route('listcoord')
    ->with('message','Questionário respondido com Sucesso');
    }     
 
     public function show(Coordenador $coord){
        return view('coord.show',compact('coord'));        
     }
 
     public function showCoord($id) { 
        $userId = Auth::id();
        $coord = Coordenador::where('id', $id)->where('user_id', $userId)->firstOrFail();
        return view('coord.show_coord',compact('coord'));        
       }

     public function edit(Coordenador $coord){
         return view('coord.edit',compact('coord'));
     }
 
     public function update(Request $request, Coordenador $coord) {
         $request->validate([
            'q1' => 'required',
            'q2' => 'required', 
            'q3' => 'required',
            'q4' => 'required', 
            'q5' => 'required',
            'q6' => 'required', 
            'q7' => 'required',
            'q8' => 'required', 
            'q9' => 'required',
            'q10' => 'required', 
            'q11' => 'required',
            'q12' => 'required',       
            'q13' => 'required',
         ]);
 
         $coord->update($request->all());
 
         
         return redirect()->route('coord.index')
                         ->with('success','Usuário Alterado com Sucesso');
 
     }
     
    public function destroy(Coordenador $coord) {
        $coord->delete();
  
        return redirect()->route('coord.index')
                        ->with('message','Coord excluído com Sucesso');
}

public function destroyCoord($id) { 
    $userId = Auth::id();
    $coord = Coordenador::where('id', $id)->where('user_id', $userId)->firstOrFail();
    $coord->delete();
    return redirect()->route('listcoord')
    ->with('message','Questionário excluído com Sucesso');      
   }
}
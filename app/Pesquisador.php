<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesquisador extends Model
{
    protected $table = "pesquisador";

    protected $fillable = [
        'user_id',   
        'q1',
        'q2',
        'q3',
        'q4',
        'q5',
        'q6',
        'q7',
        'q8',
        'q9',
        'q10',
        'q11',
        'q12',
        'q13',
        'q14',
        'q15',
        'q16',
        'q17',
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

}
 